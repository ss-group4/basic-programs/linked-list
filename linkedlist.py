class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedListIterator:
    def __init__(self, current):
        self.current = current

    def __ne__(self, other):
        return self.current != other.current

    def __iter__(self):
        return self

    def __next__(self):
        if self.current is None:
            raise StopIteration
        data = self.current.data
        self.current = self.current.next
        return data


class LinkedList:
    def __init__(self):
        self.size = 0
        self.head = None

    def __iter__(self):
        return LinkedListIterator(self.head)

    def insert(self, data, index, flag):
        s = Node(data)

        if index == -1:
            if flag == 1 and self.search(data) != -1:
                return
            s.next = self.head
            self.head = s
            self.size += 1
            return

        tmp = self.head
        prev = None
        if index == -2:
            while tmp:
                if tmp.data == data:
                    return
                prev = tmp
                tmp = tmp.next
            if prev:
                prev.next = s
            else:
                self.head = s
            self.size += 1
            return

        if index < -2 or index > self.size - 1:
            print("Invalid Index")
            return

        cur = 0
        while tmp and cur <= index:
            prev = tmp
            tmp = tmp.next
            cur += 1
        if tmp is None:
            return
        s.next = tmp
        prev.next = s
        self.size += 1

    def remove_index(self, index):
        if index == -1:
            tmp = self.head
            self.head = self.head.next
            del tmp
            self.size -= 1
            return

        tmp = self.head
        prev = None

        if index == -2:
            while tmp and tmp.next:
                prev = tmp
                tmp = tmp.next
            if prev:
                prev.next = None
                del tmp
            else:
                del self.head
                self.head = None
            self.size -= 1
            return

        cur = 0
        while tmp and cur < index:
            prev = tmp
            tmp = tmp.next
            cur += 1

        if tmp is None:
            print("Invalid Index")
            return

        if prev:
            prev.next = tmp.next
        else:
            self.head = tmp.next

        del tmp
        self.size -= 1

    def search(self, key):
        index = 0
        curr = self.head
        while curr:
            if curr.data == key:
                return index
            index += 1
            curr = curr.next
        return -1

    def to_list(self):
        vec = []
        curr = self.head
        while curr:
            vec.append(curr.data)
            curr = curr.next
        return vec

    def reverse(self):
        if self.size == 0 or self.size == 1:
            return

        cur = self.head
        prev = None
        next_node = None

        while cur:
            next_node = cur.next
            cur.next = prev
            prev = cur
            cur = next_node

        self.head = prev

    def reverse_sublist(self, node, n):
        cur = node
        prev = None
        next_node = None
        count = 0

        while cur and count < n:
            next_node = cur.next
            cur.next = prev
            prev = cur
            cur = next_node
            count += 1

        if next_node:
            node.next = self.reverse_sublist(next_node, n)

        return prev

    def reverse_n_elements(self, n):
        if self.size == 0 or self.size == 1:
            return

        cur = self.head
        prev = None
        next_node = None
        count = 0

        while cur and count < n:
            next_node = cur.next
            cur.next = prev
            prev = cur
            cur = next_node
            count += 1

        if next_node:
            self.head.next = self.reverse_sublist(next_node, n)

        self.head = prev

    def sort(self):
        for i in range(self.size):
            cur2 = self.head
            while cur2 and cur2.next:
                if cur2.data > cur2.next.data:
                    tmp = cur2.data
                    cur2.data = cur2.next.data
                    cur2.next.data = tmp
                cur2 = cur2.next

    def get_min(self):
        if not self.head:
            return None
        m = self.head.data
        for x in self:
            if m > x:
                m = x
        return m

    def get_max(self):
        if not self.head:
            return None
        m = self.head.data
        for x in self:
            if m < x:
                m = x
        return m

    def middle(self):
        count = 0
        if self.size == 0:
            return None
        for x in self:
            if count == self.size // 2:
                return x
            count += 1

    def connect_end(self):
        if self.size == 0 or self.size == 1:
            return
        node = self.head
        while node.next:
            node = node.next
        node.next = self.head
        print("The Linked list is now a circular linked list")


def join(u1, u2, flag):
    result = LinkedList()
    for x in u1:
        result.insert(x, -2, flag)

    for x in u2:
        result.insert(x, -2, flag)

    return result


def intersection(u1, u2):
    result = LinkedList()
    for x in u1:
        if u2.search(x) != -1:
            result.insert(x, -2, 1)
    return result


if __name__ == "__main__":
    linked_list = LinkedList()
    for i in range(1, 10):
        linked_list.insert(i, -1, 0)

    print("Original:", end=" ")
    for x in linked_list:
        print(x, end=" ")

    print("\nSorted List:", end=" ")
    linked_list.sort()
    for x in linked_list:
        print(x, end=" ")

    print("\nReverse N elements:", end=" ")
    linked_list.reverse_n_elements(3)
    for x in linked_list:
        print(x, end=" ")

    print()
