#include <iostream>
#include <vector>
#include<set>

using namespace std;

template<typename T>
struct Node {
    T data;
    Node<T>* next;
};

template<typename T>
class LinkedListIterator {
public:

    explicit LinkedListIterator(Node<T>* current) : current(current) {}

    bool operator!=(const LinkedListIterator<T>& other) const {
        return current != other.current;
    }

    T operator*() const {
        if (current == nullptr) {
            throw std::out_of_range("Iterator has reached the end");
        }
        return current->data;
    }

    LinkedListIterator<T>& operator++() {
        if (current != nullptr) {
            current = current->next;
        }
        return *this;
    }
    [[nodiscard]] bool hasNext() const {
        return current != nullptr;
    }

    T next() {
        if (current == nullptr) {
            throw std::out_of_range("Iterator has reached the end");
        }
        T data = current->data;
        current = current->next;
        return data;
    }


private:
    Node<T>* current;
};

template<typename T>
class LinkedList {
public:
    size_t size;
    Node<T>* head;

    [[nodiscard]] LinkedListIterator<T> begin() const {
        return LinkedListIterator<T>(head);
    }

    [[nodiscard]] LinkedListIterator<T> end() const {
        return LinkedListIterator<T>(nullptr);
    }

    void insert(T data, int index, int flag) {
        auto s = new Node<T>;
        s->data = data;
        s->next = nullptr;

        if (index == -1) {
            if(flag == 1)
            if(search(data) != -1) return;
            s->next = head;
            head = s;
            ++size;
            return;
        }

        Node<T>* tmp = head;
        Node<T>* prev = nullptr;
        if (index == -2) {
            for (; tmp != nullptr; tmp = tmp->next) {
                if(tmp->data == data) return;
                prev = tmp;
            }
            prev->next = s;

            ++size;
            return;
        }

        if (index < -2 || index > size - 1)
            cerr << "Invalid Index" << endl;

        for (int cur = 0; tmp != nullptr && cur <= index; tmp = tmp->next, cur++)
            prev = tmp;
        if (tmp == nullptr)
            return;
        s->next = tmp;
        prev->next = s;
        ++size;
    }

    void removeIndex(int index) {
        if (index == -1) {
            Node<T>* tmp = head;
            head = head->next;
            delete tmp;
            --size;
            return;
        }

        Node<T>* tmp = head;
        Node<T>* prev = nullptr;

        if (index == -2) {
            while (tmp != nullptr && tmp->next != nullptr) {
                prev = tmp;
                tmp = tmp->next;
            }

            if (prev != nullptr) {
                prev->next = nullptr;
                delete tmp;
            } else {
                delete head;
                head = nullptr;
            }

            --size;
            return;
        }

        int cur = 0;
        while (tmp != nullptr && cur < index) {
            prev = tmp;
            tmp = tmp->next;
            ++cur;
        }

        if (tmp == nullptr) {
            cerr << "Invalid Index" << endl;
            return;
        }

        if (prev != nullptr) {
            prev->next = tmp->next;
        } else {
            head = tmp->next;
        }

        delete tmp;
        --size;
    }

    int search(T key) {
        int index = 0;
        for (Node<T>* curr = head; curr != nullptr; curr = curr->next) {
            if (curr->data == key)
                return index;
            ++index;
        }
        return -1;
    }

    vector<T> toVector() {
        vector<T> vec;
        for (Node<T>* curr = head; curr != nullptr; curr = curr->next)
            vec.push_back(curr->data);

        return vec;
    }
    void reverse() {
        // If the size is 0 or 1, the list is already reversed (or empty), so return
        if (size == 0 || size == 1)
            return;

        // Initialize three pointers to keep track of the current node, its previous node, and its next node
        Node<T>* cur = head;   // Pointer to the current node
        Node<T>* prev = nullptr; // Pointer to the previous node, initially set to nullptr
        Node<T>* next = nullptr; // Pointer to the next node, initially set to nullptr

        // Traverse the linked list and reverse the links between nodes
        while (cur != nullptr) {
            next = cur->next; // Save the next node before changing the link
            // Reverse the link: Point the current node's next to the previous node
            cur->next = prev;

            // Move the 'prev' pointer to the current node, and 'cur' pointer to the next node
            prev = cur;
            cur = next;
        }

        // After the loop, 'prev' will be pointing to the last node of the original list,
        // which is now the new head of the reversed list
        head = prev;
    }
    Node<T>* reverseSublist(Node<T>* node, int n) {
        Node<T>* cur = node;
        Node<T>* prev = nullptr;
        Node<T>* next = nullptr;
        int count = 0;

        while (cur != nullptr && count < n) {
            next = cur->next;
            cur->next = prev;
            prev = cur;
            cur = next;
            count++;
        }

        if (next != nullptr)
            node->next = reverseSublist(next, n);

        return prev;
    }

    void reverse(int n) {
        // If the size is 0 or 1, the list is already reversed (or empty), so return
        if (size == 0 || size == 1)
            return;

        // Initialize three pointers to keep track of the current node, its previous node, and its next node
        Node<T>* cur = head;   // Pointer to the current node
        Node<T>* prev = nullptr; // Pointer to the previous node, initially set to nullptr
        Node<T>* next = nullptr; // Pointer to the next node, initially set to nullptr
        int count = 0;

        // Traverse the linked list and reverse the links between nodes
        while (cur != nullptr && count < n) {
            next = cur->next; // Save the next node before changing the link

            // Reverse the link: Point the current node's next to the previous node
            cur->next = prev;

            // Move the 'prev' pointer to the current node, and 'cur' pointer to the next node
            prev = cur;
            cur = next;
            count++;
        }

        // If there are more elements in the list, recursively reverse the remaining sublist
        if (next != nullptr)
            head->next = reverseSublist(next, n);

        // After the loop, 'prev' will be pointing to the first node of the reversed list,
        // which is now the new head of the reversed list
        head = prev;
    }


    void sort(){
        // implement bubble sorting on a Linked list
        for(int i =0; i < size;++i){
            for(Node<T>* cur2 = head; cur2->next != nullptr  and not is_sorted(); cur2 = cur2->next){
                if(cur2->data > cur2->next->data){
                    T tmp = cur2->data;
                    cur2->data = cur2->next->data;
                    cur2->next->data = tmp;
                }
            }
        }
    }
    T min(){
        T m = head->data;
        for(int x:this)
            if(m > x)   m = x;
        return m;

    }

    T max(){
        T m = head->data;
        for(T x:this)
            if(m < x) m = x;
        return m;
    };
    T   *middle(){
        int count = 0;
        if(size == 0) return nullptr;
        for(T x:this){
            if(count == size/2) return &x;
            count++;
        }


    }
    void connectEnd(){
        if (size == 0 || size == 1)
            return; // The list is empty or has only one node, no need to make it circular
        Node<T> * node = head;
        for(; node->next !=nullptr; node = node->next);
        node->next = head;
        cout<<" The Linked list is now a circular linked list";
    }
    bool hasCycle(){
        // Naive implementation to find cycle
        set<Node<T>*> f;
        for(Node<T>* c =  head; c !=nullptr; c = c->next) {
            if (f.find(c) != f.end())
                return true;
            f.insert(c);
        }
        return false;
    }
    bool is_sorted(){
        if(size == 0)
            return true;
        for(Node<T>* c =  head; c->next !=nullptr; c = c->next) {
            if(c->data > c->next->data && c->next != nullptr)
                return false;
        }
        return true;
    }
    void remove_duplicates(){
        if(is_sorted()){
            for(Node<T>* c =  head; c->next !=nullptr; c = c->next) {

            }

        }
    }
    bool floyd_cycle() {
        // this uses floyd_cycle algorithm. It is safe to implement on C as well.
        if (head == nullptr)
            return false;

        Node<T>* slow = head;
        Node<T>* fast = head;

        while (fast != nullptr && fast->next != nullptr) {
            slow = slow->next;
            fast = fast->next->next;

            if (slow == fast) {
                return true; // Cycle detected
            }
        }

        return false; // No cycle found
    }


};



template<typename  T>
LinkedList<T> join(LinkedList<int> u1, LinkedList<int> u2,int flag){
    LinkedList<T> result;
    for(Node<T> *cur = u1.head;  cur != nullptr; cur = cur->next)
        result.insert(cur->data, -2,flag);

    for(Node<T> *cur = u2.head;  cur != nullptr; cur = cur->next)
        result.insert(cur->data, -2,flag);

    return result;
}
template<typename  T>
LinkedList<T> intersection (LinkedList<T> u1, LinkedList<T> u2){
    LinkedList<T> result;
    for(Node<T> *cur = u1.head;
        cur != nullptr ;
        cur = cur->next){
        if(u2.search(cur->data) != -1)
            result.insert(cur->data, -2, 1);
    }
    return result;
}


int main() {
    LinkedList<int> list{};
    for (int i = 1; i < 10; i++)
        list.insert(i, -1,0);

    cout<<"Original: ";
    for(int x:list)cout<<x<<" ";

    cout << endl;

    list.sort();
    cout<<"\nSorted List: ";
    for(int x:list)cout<<x<<" ";
    cout<<"\nReverse N element: ";
    list.reverse(3);
    for(int x:list)cout<<x<<" ";
    LinkedList<int> list2{};
    for (int i = 10; i < 20; i++)
        list2.insert(i, -1,0);
    cout<<endl;
    list2.connectEnd();
    cout<<endl;
    cout<<list2.floyd_cycle();
    cout<<endl;
    cout<<list.hasCycle();
    cout  << endl;
    return 0;
}