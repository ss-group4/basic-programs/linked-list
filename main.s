	.file	"main.cpp"
	.text
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.rodata
.LC0:
	.string	" "
.LC1:
	.string	"Hello, World!"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2103:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	$0, -32(%rbp)
	movq	$0, -24(%rbp)
	movl	$0, -44(%rbp)
	jmp	.L2
.L3:
	movl	-44(%rbp), %esi
	leaq	-32(%rbp), %rax
	movl	$0, %ecx
	movl	$-1, %edx
	movq	%rax, %rdi
	call	_ZN10LinkedListIiE6insertEiii
	addl	$1, -44(%rbp)
.L2:
	cmpl	$9, -44(%rbp)
	jle	.L3
	leaq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNK10LinkedListIiE5beginEv
	movq	%rax, -40(%rbp)
	jmp	.L4
.L5:
	leaq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN18LinkedListIteratorIiE4nextEv
	movl	%eax, %esi
	leaq	_ZSt4cout(%rip), %rax
	movq	%rax, %rdi
	call	_ZNSolsEi@PLT
	movq	%rax, %rdx
	leaq	.LC0(%rip), %rax
	movq	%rax, %rsi
	movq	%rdx, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN18LinkedListIteratorIiE4nextEv
.L4:
	leaq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNK18LinkedListIteratorIiE7hasNextEv
	testb	%al, %al
	jne	.L5
	movq	_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_@GOTPCREL(%rip), %rax
	movq	%rax, %rsi
	leaq	_ZSt4cout(%rip), %rax
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E@PLT
	leaq	.LC1(%rip), %rax
	movq	%rax, %rsi
	leaq	_ZSt4cout(%rip), %rax
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_@GOTPCREL(%rip), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E@PLT
	movl	$0, %eax
	movq	-8(%rbp), %rdx
	subq	%fs:40, %rdx
	je	.L7
	call	__stack_chk_fail@PLT
.L7:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2103:
	.size	main, .-main
	.section	.rodata
.LC2:
	.string	"Invalid Index"
	.section	.text._ZN10LinkedListIiE6insertEiii,"axG",@progbits,_ZN10LinkedListIiE6insertEiii,comdat
	.align 2
	.weak	_ZN10LinkedListIiE6insertEiii
	.type	_ZN10LinkedListIiE6insertEiii, @function
_ZN10LinkedListIiE6insertEiii:
.LFB2364:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movq	%rdi, -40(%rbp)
	movl	%esi, -44(%rbp)
	movl	%edx, -48(%rbp)
	movl	%ecx, -52(%rbp)
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movl	-44(%rbp), %edx
	movl	%edx, (%rax)
	movq	-8(%rbp), %rax
	movq	$0, 8(%rax)
	cmpl	$-1, -48(%rbp)
	jne	.L9
	cmpl	$1, -52(%rbp)
	jne	.L10
	movl	-44(%rbp), %edx
	movq	-40(%rbp), %rax
	movl	%edx, %esi
	movq	%rax, %rdi
	call	_ZN10LinkedListIiE6searchEi
	cmpl	$-1, %eax
	setne	%al
	testb	%al, %al
	jne	.L22
.L10:
	movq	-40(%rbp), %rax
	movq	8(%rax), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, 8(%rax)
	movq	-40(%rbp), %rax
	movq	-8(%rbp), %rdx
	movq	%rdx, 8(%rax)
	movq	-40(%rbp), %rax
	movq	(%rax), %rax
	leaq	1(%rax), %rdx
	movq	-40(%rbp), %rax
	movq	%rdx, (%rax)
	jmp	.L8
.L9:
	movq	-40(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -24(%rbp)
	movq	$0, -16(%rbp)
	cmpl	$-2, -48(%rbp)
	jne	.L12
	jmp	.L13
.L15:
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cmpl	%eax, -44(%rbp)
	je	.L23
	movq	-24(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	-24(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -24(%rbp)
.L13:
	cmpq	$0, -24(%rbp)
	jne	.L15
	movq	-16(%rbp), %rax
	movq	-8(%rbp), %rdx
	movq	%rdx, 8(%rax)
	movq	-40(%rbp), %rax
	movq	(%rax), %rax
	leaq	1(%rax), %rdx
	movq	-40(%rbp), %rax
	movq	%rdx, (%rax)
	jmp	.L8
.L12:
	cmpl	$-2, -48(%rbp)
	jl	.L16
	movl	-48(%rbp), %eax
	movslq	%eax, %rdx
	movq	-40(%rbp), %rax
	movq	(%rax), %rax
	subq	$1, %rax
	cmpq	%rax, %rdx
	jbe	.L17
.L16:
	leaq	.LC2(%rip), %rax
	movq	%rax, %rsi
	leaq	_ZSt4cerr(%rip), %rax
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_@GOTPCREL(%rip), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E@PLT
.L17:
	movl	$0, -28(%rbp)
	jmp	.L18
.L20:
	movq	-24(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	-24(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -24(%rbp)
	addl	$1, -28(%rbp)
.L18:
	cmpq	$0, -24(%rbp)
	je	.L19
	movl	-28(%rbp), %eax
	cmpl	-48(%rbp), %eax
	jle	.L20
.L19:
	cmpq	$0, -24(%rbp)
	je	.L24
	movq	-8(%rbp), %rax
	movq	-24(%rbp), %rdx
	movq	%rdx, 8(%rax)
	movq	-16(%rbp), %rax
	movq	-8(%rbp), %rdx
	movq	%rdx, 8(%rax)
	movq	-40(%rbp), %rax
	movq	(%rax), %rax
	leaq	1(%rax), %rdx
	movq	-40(%rbp), %rax
	movq	%rdx, (%rax)
	jmp	.L8
.L22:
	nop
	jmp	.L8
.L23:
	nop
	jmp	.L8
.L24:
	nop
.L8:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2364:
	.size	_ZN10LinkedListIiE6insertEiii, .-_ZN10LinkedListIiE6insertEiii
	.section	.text._ZNK10LinkedListIiE5beginEv,"axG",@progbits,_ZNK10LinkedListIiE5beginEv,comdat
	.align 2
	.weak	_ZNK10LinkedListIiE5beginEv
	.type	_ZNK10LinkedListIiE5beginEv, @function
_ZNK10LinkedListIiE5beginEv:
.LFB2365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-24(%rbp), %rax
	movq	8(%rax), %rdx
	leaq	-16(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZN18LinkedListIteratorIiEC1EP4NodeIiE
	movq	-16(%rbp), %rax
	movq	-8(%rbp), %rdx
	subq	%fs:40, %rdx
	je	.L27
	call	__stack_chk_fail@PLT
.L27:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2365:
	.size	_ZNK10LinkedListIiE5beginEv, .-_ZNK10LinkedListIiE5beginEv
	.section	.text._ZNK18LinkedListIteratorIiE7hasNextEv,"axG",@progbits,_ZNK18LinkedListIteratorIiE7hasNextEv,comdat
	.align 2
	.weak	_ZNK18LinkedListIteratorIiE7hasNextEv
	.type	_ZNK18LinkedListIteratorIiE7hasNextEv, @function
_ZNK18LinkedListIteratorIiE7hasNextEv:
.LFB2366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	setne	%al
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2366:
	.size	_ZNK18LinkedListIteratorIiE7hasNextEv, .-_ZNK18LinkedListIteratorIiE7hasNextEv
	.section	.rodata
.LC3:
	.string	"Iterator has reached the end"
	.section	.text._ZN18LinkedListIteratorIiE4nextEv,"axG",@progbits,_ZN18LinkedListIteratorIiE4nextEv,comdat
	.align 2
	.weak	_ZN18LinkedListIteratorIiE4nextEv
	.type	_ZN18LinkedListIteratorIiE4nextEv, @function
_ZN18LinkedListIteratorIiE4nextEv:
.LFB2367:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA2367
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L31
	movl	$16, %edi
	call	__cxa_allocate_exception@PLT
	movq	%rax, %rbx
	leaq	.LC3(%rip), %rax
	movq	%rax, %rsi
	movq	%rbx, %rdi
.LEHB0:
	call	_ZNSt12out_of_rangeC1EPKc@PLT
.LEHE0:
	movq	_ZNSt12out_of_rangeD1Ev@GOTPCREL(%rip), %rax
	movq	%rax, %rdx
	leaq	_ZTISt12out_of_range(%rip), %rax
	movq	%rax, %rsi
	movq	%rbx, %rdi
.LEHB1:
	call	__cxa_throw@PLT
.L31:
	movq	-40(%rbp), %rax
	movq	(%rax), %rax
	movl	(%rax), %eax
	movl	%eax, -20(%rbp)
	movq	-40(%rbp), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	movq	-40(%rbp), %rax
	movq	%rdx, (%rax)
	movl	-20(%rbp), %eax
	jmp	.L35
.L34:
	endbr64
	movq	%rax, %r12
	movq	%rbx, %rdi
	call	__cxa_free_exception@PLT
	movq	%r12, %rax
	movq	%rax, %rdi
	call	_Unwind_Resume@PLT
.LEHE1:
.L35:
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2367:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table._ZN18LinkedListIteratorIiE4nextEv,"aG",@progbits,_ZN18LinkedListIteratorIiE4nextEv,comdat
.LLSDA2367:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2367-.LLSDACSB2367
.LLSDACSB2367:
	.uleb128 .LEHB0-.LFB2367
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L34-.LFB2367
	.uleb128 0
	.uleb128 .LEHB1-.LFB2367
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
.LLSDACSE2367:
	.section	.text._ZN18LinkedListIteratorIiE4nextEv,"axG",@progbits,_ZN18LinkedListIteratorIiE4nextEv,comdat
	.size	_ZN18LinkedListIteratorIiE4nextEv, .-_ZN18LinkedListIteratorIiE4nextEv
	.section	.text._ZN10LinkedListIiE6searchEi,"axG",@progbits,_ZN10LinkedListIiE6searchEi,comdat
	.align 2
	.weak	_ZN10LinkedListIiE6searchEi
	.type	_ZN10LinkedListIiE6searchEi, @function
_ZN10LinkedListIiE6searchEi:
.LFB2448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -24(%rbp)
	movl	%esi, -28(%rbp)
	movl	$0, -12(%rbp)
	movq	-24(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -8(%rbp)
	jmp	.L37
.L40:
	movq	-8(%rbp), %rax
	movl	(%rax), %eax
	cmpl	%eax, -28(%rbp)
	jne	.L38
	movl	-12(%rbp), %eax
	jmp	.L39
.L38:
	addl	$1, -12(%rbp)
	movq	-8(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -8(%rbp)
.L37:
	cmpq	$0, -8(%rbp)
	jne	.L40
	movl	$-1, %eax
.L39:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2448:
	.size	_ZN10LinkedListIiE6searchEi, .-_ZN10LinkedListIiE6searchEi
	.section	.text._ZN18LinkedListIteratorIiEC2EP4NodeIiE,"axG",@progbits,_ZN18LinkedListIteratorIiEC5EP4NodeIiE,comdat
	.align 2
	.weak	_ZN18LinkedListIteratorIiEC2EP4NodeIiE
	.type	_ZN18LinkedListIteratorIiEC2EP4NodeIiE, @function
_ZN18LinkedListIteratorIiEC2EP4NodeIiE:
.LFB2450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rdx
	movq	%rdx, (%rax)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2450:
	.size	_ZN18LinkedListIteratorIiEC2EP4NodeIiE, .-_ZN18LinkedListIteratorIiEC2EP4NodeIiE
	.weak	_ZN18LinkedListIteratorIiEC1EP4NodeIiE
	.set	_ZN18LinkedListIteratorIiEC1EP4NodeIiE,_ZN18LinkedListIteratorIiEC2EP4NodeIiE
	.text
	.type	_Z41__static_initialization_and_destruction_0ii, @function
_Z41__static_initialization_and_destruction_0ii:
.LFB2614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	cmpl	$1, -4(%rbp)
	jne	.L44
	cmpl	$65535, -8(%rbp)
	jne	.L44
	leaq	_ZStL8__ioinit(%rip), %rax
	movq	%rax, %rdi
	call	_ZNSt8ios_base4InitC1Ev@PLT
	leaq	__dso_handle(%rip), %rax
	movq	%rax, %rdx
	leaq	_ZStL8__ioinit(%rip), %rax
	movq	%rax, %rsi
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rax
	movq	%rax, %rdi
	call	__cxa_atexit@PLT
.L44:
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2614:
	.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destruction_0ii
	.type	_GLOBAL__sub_I_main, @function
_GLOBAL__sub_I_main:
.LFB2615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$65535, %esi
	movl	$1, %edi
	call	_Z41__static_initialization_and_destruction_0ii
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2615:
	.size	_GLOBAL__sub_I_main, .-_GLOBAL__sub_I_main
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_main
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.data.rel.local.DW.ref.__gxx_personality_v0,"awG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.align 8
	.type	DW.ref.__gxx_personality_v0, @object
	.size	DW.ref.__gxx_personality_v0, 8
DW.ref.__gxx_personality_v0:
	.quad	__gxx_personality_v0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 11.3.0-1ubuntu1~22.04.1) 11.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	1f - 0f
	.long	4f - 1f
	.long	5
0:
	.string	"GNU"
1:
	.align 8
	.long	0xc0000002
	.long	3f - 2f
2:
	.long	0x3
3:
	.align 8
4:
